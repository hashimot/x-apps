'use strict';

module.exports = function(app) {
    var toDoList = require('./Controllers/ToDoController.js');
    app.route('/task/show-all').
        get(toDoList.list_all_tasks);
    app.route('/task/insert').
        post(toDoList.new_task);
    app.route('/task/:_id').
        delete(toDoList.delete_task);
}
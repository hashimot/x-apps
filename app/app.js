'use strict';

// Declare app level module which depends on views, and components
angular.module('app', [
  'ngRoute',
]).
config(['$locationProvider', '$routeProvider', '$httpProvider', function($locationProvider, $routeProvider, $httpProvider) {
        $routeProvider.when('/', {
            templateUrl: 'views/api/index.html',
            controller: 'ApiController'
        }).when('/first-question', {
            templateUrl: 'views/api/index.html',
            controller: 'ApiController'
        }).when('/second-question', {
            templateUrl: 'views/second-question/index.html',
            controller: 'SecondQuestionController'
        });
}]);

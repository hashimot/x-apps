var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const ToDoListSchema = new Schema({
    description: { type: String },
    status: {
        type: String,
        enum: ['concluida', 'pendente']
    }
}, {
    timestamps: true
});

const ToDoList = mongoose.model('ToDoList', ToDoListSchema);
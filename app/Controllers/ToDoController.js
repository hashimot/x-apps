var mongoose = require('mongoose');
var Task = mongoose.model('ToDoList');

exports.list_all_tasks = function(req, res){
    Task.find({}, function(err, tasks) {
        if (err){ res.send(err); }
        res.json(tasks);
    });
};

exports.new_task = function(req, res){
    var new_task = new Task();
    new_task.description = req.body.description;
    new_task.status = req.body.status;

    new_task.save(function(err, task) {
        if (err){ res.send(err); } else {
            res.json(task);
        }
    });
};

exports.delete_task = function(req, res){
    var task = {};
    task._id = req.params._id;
    Task.remove(
        { _id: task._id },
        function(err, task){
            if(err) res.send(err)
            res.json({
                success: true
            });
        }
    )
};
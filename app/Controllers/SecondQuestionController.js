var app = angular.module('app');
app.controller('SecondQuestionController', ['$scope', 'TaskFactory', function($scope, TaskFactory){
    $scope.error = false;
    $scope.task = new Task();
    $scope.taskList = [];

    showTasks();

    $scope.submit = function(){
        TaskFactory.insert($scope.task).then(function(result){
            if(result.data._id){
                $scope.error = false;
                $scope.taskList.push(result.data);
            } else {

                $scope.error = 'Erro ao inserir';
            }
        });
    };
    $scope.remove = function(_id){
        TaskFactory.remove(_id).then(function(result){
            if(result.data.success === true){
                showTasks();
            }
        });
    };
    function Task(){
        return {
            description: '',
            status: ['concluida', 'pendente']
        }
    }
    function showTasks(){
        TaskFactory.showAll().then(function(result){
            $scope.taskList =result.data;
        });
    }
}]);
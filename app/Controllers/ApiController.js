var app = angular.module('app');
app.controller('ApiController', ['$scope', 'ApiFactory',  function($scope, ApiFactory){
    $scope.pages = 0;
    $scope.has = {
        error: false,
        message: ''
    };
    $scope.dataToShow = [];

    ApiFactory.getTotalPage().then(function(result){
        var data = result.data;
        if(data.length > 0){
            $scope.pages = new Array(data[0].total_pages);
        }
    });

    $scope.showPerPage = function(pageNumber){
        if(typeof pageNumber !== 'number'){
            $scope.has = {
                error: true,
                message: 'Page Number must be integer'
            };
        } else {
            ApiFactory.showPerPage(pageNumber).then(function(result){
                var data = result.data;
                var datalength = data.length;
                var dataToShow = [];
                if(data.error === true){
                    $scope.hasError = true;
                } else {
                    if(datalength > 0){
                        for(var i = 0; i < datalength; i++){
                            if(data[i].page == pageNumber){
                                dataToShow = data[i].data;
                                break;
                            }
                        }
                        $scope.dataToShow = dataToShow;
                    }
                }
            }).error(function(error){
               console.log(error)
            });
        }
    }
}]);
var app = angular.module('app');
app.factory('ApiFactory', ['$http', function($http){
    var ApiFactory = {};
    var apiUrl = "http://reqres.in/api";
    ApiFactory.getTotalPage = function(){
        return $http({
            method: 'GET',
            url: 'json/api-reqres.json'
        });
    };
    ApiFactory.showPerPage = function(page){

/*
        //está dando um problema no CORS, tentei de diversas formas encontrar o problema, mas não consegui resolve-lo;
        return $http({
            url: apiUrl + '/users',
            method: 'GET',
            params: {
                page: page
            },
            headers: {'Access-Control-Allow-Origin': '*'}
        });*/
        return $http({
            method: 'GET',
            url: 'json/api-reqres.json'
        });
    };

    return ApiFactory;
}]);
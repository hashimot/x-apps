var app = angular.module('app');
app.factory('TaskFactory', ['$http', function($http){
    var TaskFactory = {};
    var url = 'task';
    TaskFactory.insert = function(data){
        return $http.post(url + '/' + 'insert', data);
    };
    TaskFactory.showAll = function(){
        return $http.get(url + '/show-all');
    };
    TaskFactory.remove = function(_id){
        return $http.delete(url + '/' + _id);
    };
    return TaskFactory;
}]);